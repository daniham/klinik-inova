<?php
require_once 'app/functions/MY_model.php';
$Users = get("SELECT * FROM users");

$no = 1;

?>

<!-- User Table -->
<section id="column-selectors">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">User</h4>
          <a href="?page=tambah-users" class="btn btn-primary round waves-effect waves-light">
            Tambah users
          </a>
        </div>
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table table-striped dataex-html5-selectors">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($Users as $users) : ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $users['username']; ?></td>
                      <td><?= $users['password']; ?></td>
                      <td>
                        <a href="?page=edit-users&id=<?= $users['id']; ?>"><i class="m-1 feather icon-edit-2"></i></a>
                        <a href="?page=hapus-users&id=<?= $users['id']; ?>" class="btn-hapus"><i class="feather icon-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- User Table -->
<?php $title = 'users'; ?>