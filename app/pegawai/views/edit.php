<?php
require_once 'app/functions/MY_model.php';

$id = $_GET['id'];
$pegawai = get_where("SELECT * FROM pegawai WHERE id = '$id' ");

?>
<div class="content-header row">

  <div class="content-header-right col-md-12">
    <a href="?page=pegawai" class="btn btn-light float-right mb-2">Kembali</a>
  </div>
</div>
<section id="basic-horizontal-layouts">
  <div class="row match-height">
    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Edit Pegawai</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form action="app/pegawai/proses/update.php" method="post">
              <input type="hidden" name="id" value="<?= $pegawai['id']; ?>">
              <div class="form-body">
                <div class="row">
                  
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Nik </label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Nik Pegawai" class="form-control" name="nik" value="<?= $pegawai['nik']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Nama </label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Nama Pegawai" class="form-control" name="nama" value="<?= $pegawai['nama']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Alamat</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="alamat" class="form-control" name="alamat" value="<?= $pegawai['alamat']; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Jenis Kelamin</label>
                      </div>
                      <div class="col-md-8">
                        <ul class="list-unstyled mb-0">
                          <li class="d-inline-block mr-2">
                            <fieldset>
                              <div class="vs-radio-con">
                                <input type="radio" <?= ($pegawai['jk'] == 'l' ? 'checked' : ''); ?> name="jk" value="l">
                                <span class="vs-radio">
                                  <span class="vs-radio--border"></span>
                                  <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Laki-laki</span>
                              </div>
                            </fieldset>
                          </li>
                          <li class="d-inline-block mr-2">
                            <fieldset>
                              <div class="vs-radio-con">
                                <input type="radio" name="jk" <?= ($pegawai['jk'] == 'p' ? 'checked' : ''); ?> value="p">
                                <span class="vs-radio">
                                  <span class="vs-radio--border"></span>
                                  <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Perempuan</span>
                              </div>
                            </fieldset>
                          </li>
                        </ul>
                      </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Tgl Lahir</label>
                      </div>
                      <div class="col-md-8">
                        <input type="date" placeholder="tgl_lahir" class="form-control" name="tgl_lahir" value="<?= $pegawai['tgl_lahir']; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Email</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="email" class="form-control" name="email" value="<?= $pegawai['email']; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Phone</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Phone" class="form-control" name="no_tlp" value="<?= $pegawai['no_tlp']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Jabatan</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Jabatan" class="form-control" name="jabatan" value="<?= $pegawai['jabatan']; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-md-8 offset-md-4">
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>