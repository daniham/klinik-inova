<?php
require_once 'app/functions/MY_model.php';
$obats = get("SELECT * FROM pegawai");

$no = 1;

?>

<!-- User Table -->
<section id="column-selectors">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Pegawai</h4>
          <a href="?page=tambah-pegawai" class="btn btn-primary round waves-effect waves-light">
            Tambah pegawai
          </a>
        </div>
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table table-striped dataex-html5-selectors">
                <thead>
                  <tr>
                    <th></th>
                    <th>Nik</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th>Tgl Lahir</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Jabatan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($obats as $pegawai) : ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $pegawai['nik']; ?></td>
                      <td><?= $pegawai['nama']; ?></td>
                      <td><?= $pegawai['alamat']; ?></td>
                      <td><?= ($pegawai['jk'] == 'l' ? 'Laki-laki' : 'Perempuan'); ?></td>
                      <td><?= $pegawai['tgl_lahir']; ?></td>
                      <td><?= $pegawai['email']; ?></td>
                      <td><?= $pegawai['no_tlp']; ?></td>
                      <td><?= $pegawai['jabatan']; ?></td>
                      <td>
                        <a href="?page=edit-pegawai&id=<?= $pegawai['id']; ?>"><i class="m-1 feather icon-edit-2"></i></a>
                        <a href="?page=hapus-pegawai&id=<?= $pegawai['id']; ?>" class="btn-hapus"><i class="feather icon-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- User Table -->
<?php $title = 'pegawai'; ?>