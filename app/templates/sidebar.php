<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="html/ltr/vertical-menu-template-semi-dark/index.html">
          <div class="brand-logo"></div>
          <h2 class="brand-text mb-0">Klinik</h2>
        </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="<?php echo is_active(''); ?> nav-item">

        <a href="index.php"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        </li>
        <li class="<?php echo is_active(''); ?> nav-item">
        </li>  
            <li>
                <a href="#"><i class="fa fa-tags fa-fw"></i>Master<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="?page=wilayah">Wilayah</a>
                  </li>
                  <li>
                      <a href="?page=user">User</a>
                  </li>
                  <li>
                      <a href="?page=pegawai">Pegawai</a>
                  </li>
                  <li>
                      <a href="?page=tindakan">Tindakan</a>
                  </li>
                  <li>
                      <a href="?page=obat">Obat</a>
                  </li>
                  <li>
                      <a href="?page=ruang">Ruang</a>
                  </li>
                  <li>
                      <a href="?page=dokter">Dokter</a>
                  </li>
                </ul>
             </li>

             <li>
                <a href="#"><i class="feather icon-activity"></i>Transaksi<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                      <li>
                          <a href="?page=pasien">Pendaftaran Pasien</a>
                      </li>
                      <li>
                          <a href="morris.html">Tindakan Pasien</a>
                      </li>
                      <li>
                          <a href="morris.html">Obat Pasien</a>
                      </li>
                  </ul>
              </li>
    
      <li class="<?php echo is_active('rekam-medis'); ?> nav-item"><a href="?page=rekam-medis"><i class="feather icon-bar-chart"></i><span class="menu-title" data-i18n="Rekam Medis">Rekam Medis</span></a>

      <li class="nav-item"><a href="javascript:;"><i class="feather icon-pie-chart"></i><span class="menu-title" data-i18n="Laporan">Laporan</span></a>
        <ul class="menu-content">
          <li class="<?php echo is_active('lap-rekam-medis'); ?>"><a href="?page=lap-rekam-medis"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Rekam Medis">Rekam Medis</span></a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>
<!-- END: Main Menu-->