<?php
if (isset($_GET['page'])) {

	switch ($_GET['page']) {
		case 'user':
			require_once 'app/user/views/index.php';
			break;
		case 'tambah-user':
			require_once 'app/user/views/create.php';
			break;
		case 'edit-user':
			require_once 'app/user/views/edit.php';
			break;
		case 'hapus-user':
			require_once 'app/user/proses/delete.php';
			break;

		case 'dokter':
			require_once 'app/dokter/views/index.php';
			break;
		case 'tambah-dokter':
			require_once 'app/dokter/views/create.php';
			break;
		case 'edit-dokter':
			require_once 'app/dokter/views/edit.php';
			break;
		case 'hapus-dokter':
			require_once 'app/dokter/proses/delete.php';
			break;

		case 'pasien':
			require_once 'app/pasien/views/index.php';
			break;
		case 'tambah-pasien':
			require_once 'app/pasien/views/create.php';
			break;
		case 'edit-pasien':
			require_once 'app/pasien/views/edit.php';
			break;
		case 'hapus-pasien':
			require_once 'app/pasien/proses/delete.php';
			break;

		case 'obat':
			require_once 'app/obat/views/index.php';
			break;
		case 'tambah-obat':
			require_once 'app/obat/views/create.php';
			break;
		case 'edit-obat':
			require_once 'app/obat/views/edit.php';
			break;
		case 'hapus-obat':
			require_once 'app/obat/proses/delete.php';
			break;

		case 'tindakan':
			require_once 'app/tindakan/views/index.php';
			break;
		case 'tambah-tindakan':
			require_once 'app/tindakan/views/create.php';
			break;
		case 'edit-tindakan':
			require_once 'app/tindakan/views/edit.php';
			break;
		case 'hapus-tindakan':
			require_once 'app/tindakan/proses/delete.php';
			break;

		case 'pegawai':
			require_once 'app/pegawai/views/index.php';
			break;
		case 'tambah-pegawai':
			require_once 'app/pegawai/views/create.php';
			break;
		case 'edit-pegawai':
			require_once 'app/pegawai/views/edit.php';
			break;
		case 'hapus-pegawai':
			require_once 'app/pegawai/proses/delete.php';
			break;

		case 'wilayah':
			require_once 'app/wilayah/views/index.php';
			break;
		case 'tambah-wilayah':
			require_once 'app/wilayah/views/create.php';
			break;
		case 'edit-wilayah':
			require_once 'app/wilayah/views/edit.php';
			break;
		case 'hapus-wilayah':
			require_once 'app/wilayah/proses/delete.php';
			break;

		case 'ruang':
			require_once 'app/ruang/views/index.php';
			break;
		case 'tambah-ruang':
			require_once 'app/ruang/views/create.php';
			break;
		case 'edit-ruang':
			require_once 'app/ruang/views/edit.php';
			break;
		case 'hapus-ruang':
			require_once 'app/ruang/proses/delete.php';
			break;

		case 'rekam-medis':
			require_once 'app/rekam-medis/views/index.php';
			break;
		case 'tambah-rekam-medis':
			require_once 'app/rekam-medis/views/create.php';
			break;
		case 'edit-rekam-medis':
			require_once 'app/rekam-medis/views/edit.php';
			break;
		case 'hapus-rekam-medis':
			require_once 'app/rekam-medis/proses/delete.php';
			break;
		case 'lap-rekam-medis':
			require_once 'app/laporan/views/rekam-medis.php';
			break;
	}
} else {
	require_once 'app/dashboard/views/index.php';
}
