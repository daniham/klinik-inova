<?php
require_once 'app/functions/MY_model.php';

$id = $_GET['id'];
$wilayah = get_where("SELECT * FROM wilayah WHERE id = '$id' ");

?>
<div class="content-header row">

  <div class="content-header-right col-md-12">
    <a href="?page=wilayah" class="btn btn-light float-right mb-2">Kembali</a>
  </div>
</div>
<section id="basic-horizontal-layouts">
  <div class="row match-height">
    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Edit Wilayah</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form action="app/wilayah/proses/update.php" method="post">
              <input type="hidden" name="id" value="<?= $wilayah['id']; ?>">
              <div class="form-body">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Nama Wilayah</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Nama Wilayah" class="form-control" name="nama_wil" value="<?= $wilayah['nama_wil']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kelurahan</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Kelurahan" class="form-control" name="kelurahan" value="<?= $wilayah['kelurahan']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kabupaten/Kota</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Kecamatan" class="form-control" name="kecamatan" value="<?= $wilayah['kecamatan']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kabupaten/Kota</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Kabupaten/Kota" class="form-control" name="kabkot" value="<?= $wilayah['kabkot']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Provinsi</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Provinsi" class="form-control" name="provinsi" value="<?= $wilayah['provinsi']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-8 offset-md-4">
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>