<?php
require_once 'app/functions/MY_model.php';
$obats = get("SELECT * FROM wilayah");

$no = 1;

?>

<!-- User Table -->
<section id="column-selectors">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Wilayah</h4>
          <a href="?page=tambah-wilayah" class="btn btn-primary round waves-effect waves-light">
            Tambah wilayah
          </a>
        </div>
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table table-striped dataex-html5-selectors">
                <thead>
                  <tr>
                    <th></th>
                    <th>Nama Wilayah</th>
                    <th>Kelurahan</th>
                    <th>Kecamatan</th>
                    <th>Kabupaten/Kota</th>
                    <th>Provinsi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($obats as $wilayah) : ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $wilayah['nama_wil']; ?></td>
                      <td><?= $wilayah['kelurahan']; ?></td>
                      <td><?= $wilayah['kecamatan']; ?></td>
                      <td><?= $wilayah['kabkot']; ?></td>
                      <td><?= $wilayah['provinsi']; ?></td>
                      <td>
                        <a href="?page=edit-wilayah&id=<?= $wilayah['id']; ?>"><i class="m-1 feather icon-edit-2"></i></a>
                        <a href="?page=hapus-wilayah&id=<?= $wilayah['id']; ?>" class="btn-hapus"><i class="feather icon-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- User Table -->
<?php $title = 'wilayah'; ?>