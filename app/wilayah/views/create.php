<div class="content-header row">

  <div class="content-header-right col-md-12">
    <a href="?page=wilayah" class="btn btn-light float-right mb-2">Kembali</a>
  </div>
</div>
<section id="basic-horizontal-layouts">
  <div class="row match-height">
    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tambah Wilayah</h4>
        </div>
        <div class="card-content">
          <div class="card-body">
            <form action="app/wilayah/proses/create.php" method="post">
              <div class="form-body">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Nama Wilayah </label>
                      </div>
                      <div class="col-md-8">

                        <input type="text" placeholder="Nama Wilayah" class="form-control" name="nama_wil" required>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kelurahan</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="kelurahan" class="form-control" name="kelurahan" required>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kecamatan</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="kecamatan" class="form-control" name="kecamatan" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Kabupaten/Kota</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="Kabupaten./Kota" class="form-control" name="kabkot" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Provinsi</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" placeholder="provinsi" class="form-control" name="provinsi" required>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>